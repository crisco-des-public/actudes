# Éléments de recherche extraits des lettres d'actualités du Dictionnaire Electronique des Synonymes (DES) du CRISCO

## de la lettre n°1 (janvier 2018) à la lettre n°19 (Février 2023)


Accès au document au [format markdown](actudes.md)

Accès au document au format [html](actudes.html)

Accès au document au format [PDF](actudes.pdf)


Création du fichier html :

pandoc --standalone --template template_actudes.html --toc --css actudes.css -f markdown actudes.md -o actudes.html

Création du fichier pdf :


weasyprint actudes.html actudes.pdf

ou

Dans le navigateur, charger actudes.html pui menu fichier / imprimer en pdf